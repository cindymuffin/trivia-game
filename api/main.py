from sre_parse import CATEGORIES
from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
import psycopg

app = FastAPI()


@app.get("/api/categories")
def list_categories(page: int = 0):
    conn_str = "postgresql://trivia-game:trivia-game@db/trivia-game"
    with psycopg.connect(conn_str) as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT id, title, canon
                FROM categories
                LIMIT 100 OFFSET %s
                """,
                [page * 100],
            )
            records = cur.fetchall()
            results = []
            for record in records:
                result = {
                    "id": record[0],
                    "title": record[1],
                    "canon": record[2],
                }
                results.append(result)
            return results


@app.get("/api/categories/{category_id}")
def category_detail(category_id: int):
    conn_str = "postgresql://trivia-game:trivia-game@db/trivia-game"
    with psycopg.connect(conn_str) as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT id, title, canon
                FROM categories
                WHERE id = %s
                """,
                [category_id],
            )
            record = cur.fetchone()
            return {
                "id": record[0],
                "title": record[1],
                "canon": record[2],
            }


class Category(BaseModel):
    title: str
    canon: bool


@app.post("/api/categories")
def create_category(category: Category):
    return category


@app.put("/api/categories/{category_id}")
def update_category(category_id: int, category_title: str):
    update_category_encoded = jsonable_encoder(category_title)
    return update_category_encoded


@app.delete("/api/categories/{category_id}")
def delete_category(category_id: int):
    conn_str = "postgresql://trivia-game:trivia-game@db/trivia-game"
    with psycopg.connect(conn_str) as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                DELETE FROM categories
                WHERE id = %s
                """,
                [category_id],
            )
            return "Category deleted"
